﻿using Newtonsoft.Json;
using SensorManagerTester.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorManagerTester.Utils
{
	public class PicoInterface
	{
		private static readonly string BASE_EVENT_URI = "http://localhost:8080/sky/event/";

		private static readonly string BASE_QUERY_URI = "http://localhost:8080/sky/cloud/";

		private static readonly string BASE_PICO_ECI = "29rcuoS1ZjXCmH85pTjb1N/";

		private static readonly string CONTENT_TYPE = "application/json";

		public static void CreateSensorPico(string name)
		{
			WebRequest request = new WebRequest();
			var sensorPico = new SensorPico
			{
				Name = name
			};
			var sensorPicoSerialized = JsonConvert.SerializeObject(sensorPico);
			string response = request.Post(BASE_EVENT_URI + BASE_PICO_ECI + GenerateURIUUID() + "sensor/" + "new_sensor", sensorPicoSerialized, CONTENT_TYPE);
		}

		public static List<SensorPico> GetAllSensorPicos()
		{
			WebRequest request = new WebRequest();
			string response = request.Get(BASE_QUERY_URI + BASE_PICO_ECI + "manage_sensors/" + "sensors");
			return JsonConvert.DeserializeObject<List<SensorPico>>(response);
		}

		public static void DeleteSensorPico(string name)
		{
			WebRequest request = new WebRequest();
			var sensorPico = new SensorPico
			{
				Name = name
			};
			var sensorPicoSerialized = JsonConvert.SerializeObject(sensorPico);
			string response = request.Post(BASE_EVENT_URI + BASE_PICO_ECI + GenerateURIUUID() + "sensor/" + "unneeded_sensor", sensorPicoSerialized, CONTENT_TYPE);
		}

		public static List<TemperatureReading> ReadAllTemperatures()
		{
			WebRequest request = new WebRequest();
			string response = request.Get(BASE_QUERY_URI + BASE_PICO_ECI + "manage_sensors/" + "temperatures");
			return JsonConvert.DeserializeObject<List<TemperatureReading>>(response);
		}

		public static TemperatureProfile ReadSensorPicoProfile(string name)
		{
			WebRequest request = new WebRequest();
			string response = request.Get(BASE_QUERY_URI + GetECI(name) + "/sensor_profile/" + "profile");
			return JsonConvert.DeserializeObject<TemperatureProfile>(response);
		}

		public static void SendTemperatureReadingToSensorPico(string name, string temperatureReading)
		{
			WebRequest request = new WebRequest();
			string response = request.Post(BASE_EVENT_URI + GetECI(name) + "/" + GenerateURIUUID() + "wovyn/" + "heartbeat", temperatureReading, CONTENT_TYPE);
		}

		public static void SetSensorPicoProfile(TemperatureProfile temperatureProfile)
		{
			WebRequest request = new WebRequest();
			var requestString = JsonConvert.SerializeObject(temperatureProfile);
			string response = request.Post(BASE_EVENT_URI + GetECI(temperatureProfile.Name) + "/" + GenerateURIUUID() + "sensor/" + "profile_updated", requestString, CONTENT_TYPE);
		}

		private static string GetECI(string name)
		{
			var sensorPicos = GetAllSensorPicos();
			foreach (var sensorPico in sensorPicos)
			{
				if (sensorPico.Name.Equals(name))
				{
					return sensorPico.ECI;
				}
			}
			return null;
		}

		private static string GenerateURIUUID()
		{
			return Guid.NewGuid().ToString() + "/";
		}
	}
}
