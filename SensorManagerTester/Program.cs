﻿using SensorManagerTester.Models;
using SensorManagerTester.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorManagerTester
{
	class Program
	{
		private static readonly string SUCCESS_MESSAGE = "Test Passed";
		private static readonly string FAIL_MESSAGE = "Test Failed";
		private static bool FailedTest = false;

		static void Main(string[] args)
		{
			System.Threading.Thread.Sleep(5000);

			// Test creation/duplication
			PicoInterface.CreateSensorPico("TestingPico1");
			PicoInterface.CreateSensorPico("TestingPico1");
			System.Threading.Thread.Sleep(1000);

			Assert(PicoInterface.GetAllSensorPicos().Count == 1, SUCCESS_MESSAGE, FAIL_MESSAGE);

			// Test deletion
			PicoInterface.DeleteSensorPico("TestingPico1");
			System.Threading.Thread.Sleep(1000);

			Assert(PicoInterface.GetAllSensorPicos().Count == 0, SUCCESS_MESSAGE, FAIL_MESSAGE);

			// Test multiple creation
			PicoInterface.CreateSensorPico("TestingPico1");
			PicoInterface.CreateSensorPico("TestingPico2");
			PicoInterface.CreateSensorPico("TestingPico3");
			PicoInterface.CreateSensorPico("TestingPico4");
			PicoInterface.CreateSensorPico("TestingPico5");

			// Wait for all the picos to be created
			System.Threading.Thread.Sleep(1000);

			var sensorPicos = PicoInterface.GetAllSensorPicos();
			Assert(sensorPicos.Count == 5, SUCCESS_MESSAGE, FAIL_MESSAGE);

			// Give a temperature reading to each one
			foreach (var sensorPico in sensorPicos)
			{
				var temperatureReading = "{\"genericThing\":{\"data\":{\"temperature\":[{\"temperatureF\":70.24,\"temperatureC\":21.8}]}}}";
				PicoInterface.SendTemperatureReadingToSensorPico(sensorPico.Name, temperatureReading);
			}
			System.Threading.Thread.Sleep(1000);

			// Make sure the readings are there, and the management pico can get them
			Assert(PicoInterface.ReadAllTemperatures().Count == sensorPicos.Count, SUCCESS_MESSAGE, FAIL_MESSAGE);

			// Delete a pico and make sure its reading is gone, but the temperature readings still work
			PicoInterface.DeleteSensorPico("TestingPico1");
			System.Threading.Thread.Sleep(1000);

			Assert(PicoInterface.ReadAllTemperatures().Count == sensorPicos.Count - 1, SUCCESS_MESSAGE, FAIL_MESSAGE);

			// Get the sensor profile and make sure it was set properly
			var sensorPicoProfile = PicoInterface.ReadSensorPicoProfile("TestingPico2");
			System.Threading.Thread.Sleep(1000);
			Assert(sensorPicoProfile.Location.Equals("Provo"), SUCCESS_MESSAGE, FAIL_MESSAGE);

			// Change the sensor profile and make sure it updates
			var newLocation = "Some place over the moon";
			sensorPicoProfile.Location = newLocation;
			PicoInterface.SetSensorPicoProfile(sensorPicoProfile);
			System.Threading.Thread.Sleep(1000);
			Assert(sensorPicoProfile.Location.Equals(newLocation), SUCCESS_MESSAGE, FAIL_MESSAGE);

			foreach (var sensorPico in PicoInterface.GetAllSensorPicos())
			{
				PicoInterface.DeleteSensorPico(sensorPico.Name);
			}

			Assert(!FailedTest, "All Tests Passed", "Not All Tests Passed");
		}


		static void Assert(bool assertion, string successMessage, string failMessage)
		{
			if (assertion)
			{
				Console.WriteLine(successMessage);
			}
			else
			{
				FailedTest = true;
				Console.WriteLine(failMessage);
			}
		}
	}
}
