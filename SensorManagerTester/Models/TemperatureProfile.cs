﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SensorManagerTester.Models
{
	public class TemperatureProfile
	{
		public string Name { get; set; }
		public string Location { get; set; }
		public double ThresholdTemperature { get; set; }
		public string ToSMSNumber { get; set; }
		public string FromSMSNumber { get; set; }
	}
}