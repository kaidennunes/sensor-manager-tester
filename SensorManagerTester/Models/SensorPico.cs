﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorManagerTester.Models
{
	public class SensorPico
	{
		public string Name { get; set; }
		public string ECI { get; set; }
	}
}
