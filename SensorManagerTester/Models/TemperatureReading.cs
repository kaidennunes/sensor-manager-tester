﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SensorManagerTester.Models
{
	public class TemperatureReading
	{
		[JsonProperty(PropertyName = "id")]
		public int ID { get; set; }

		[JsonProperty(PropertyName = "temperature")]
		public double Temperature { get; set; }

		[JsonProperty(PropertyName = "timestamp")]
		public string TimeStamp { get; set; }

		[JsonIgnore]
		public bool IsTemperatureViolation { get; set; }
	}
}