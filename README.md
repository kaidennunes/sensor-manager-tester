Wovyn Sensor Manager Tester Harness

To run: 
1. Open solution file (.sln)
2. Restore nuget packages
3. Modify the ECI for the wovyn sensor manager pico
4. Click ctr + F5 to run test.

NOTE: The sensor manager pico must not have any children picos when running the test harness (i.e. you must start with a clean slate).